import React from 'react'

const footer = () => {
  return (
    <>
        <section className="foter">
            <ul>
                <li>Jalan Suroyo No. 161 Mayangan Kota Probolinggo 672000</li>
                <li>binarcarrental@gmail.com</li>
                <li>081-233-334-808</li>
            </ul>

            <ul>
                <li>Our services</li>
                <li>Why Us</li>
                <li>Testimonial</li>
                <li>FAQ</li>
            </ul>

            <div>
                <h5>Connect with us</h5>
                <div>
                    <img src="/images/icon_facebook.svg" alt="facebook" />
                    <img src="/images/icon_instagram.svg" alt="instagram" />
                    <img src="/images/icon_twitter.svg" alt="twitter" />
                    <img src="/images/icon_mail.svg" alt="mail" />
                    <img src="/images/icon_twitch.svg" alt="twitch" />
                </div>
            </div>

            <div>
                <h5>Copyright Binar 2022</h5>
                <img src="/images/logo.svg" alt="Logo Binar Car Rental" />
            </div>
        </section>
    </>
  )
}

export default footer