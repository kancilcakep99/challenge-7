import React from 'react'
import { Pie } from 'react-chartjs-2';
// eslint-disable-next-line
import { Chart } from "chart.js/auto";

const service = () => {
  const dataChart = {
    labels: ["Innova", "APV", "Ferarri", "Avanza", "Jazz", "Kijang"],
    datasets: [
      {
        label: "banyak peminat",
        backgroundColor: ["blue", "green", "purple", "red", "yellow", "gray"],
        data: [5, 10, 61, 9, 12, 3],
      },
    ]
  }
  return (
    <>
        <section className="our-services">
            <div className="chart">
                <Pie data={dataChart} />
            </div>
        </section>
    </>
  )
}

export default service