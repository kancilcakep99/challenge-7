import React from 'react'
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import Header from '../pages/header';
import Footer from './footer'
import Head from 'next/head';
import { Button } from 'react-bootstrap';

const Car = () => {
    const [value, setValue] = React.useState(null);

    const driver = [
      { label: 'Dengan Sopir' },
      { label: 'Tanpa Sopir (Lepas Kunci)' },
    ];

    const penumpang = [
      { label: '1 orang'},
      { label: '2 orang'},
      { label: '3 orang'},
      { label: '4 orang'},  
    ]
  return (
    <>
      <Head>
        <title>Binar Car Rental</title>
        <meta name="description" content="Binar Car Rental - Cari Mobil" />
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossorigin="anonymous"
        />
      </Head>

      <Header />
      <section className="type">
        <div className="input">
          <div>
            <h6 className="driver">Tipe Driver</h6>
            <Autocomplete
              className="date-driver"
              disablePortal
              id="combo-box-demo"
              options={driver}
              sx={{ width: 210.75 }}
              renderInput={(params) => <TextField {...params} label="Pilih Tipe Driver" />}
            />
          </div>

          <div>
            <h6 className="driver">Tanggal</h6>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                className="date-driver"
                label="Basic example"
                value={value}
                onChange={(newValue) => {
                setValue(newValue);
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
          </div>

          <div>
            <h6 className="driver">Waktu Jemput/Ambil</h6>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <TimePicker
                className="date-driver"
                label="Basic example"
                value={value}
                onChange={(newValue) => {
                  setValue(newValue);
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
          </div>

          <div>
            <h6 className="driver">Jumlah Penumpang (optional)</h6>
            <Autocomplete
              className="date-driver"
              disablePortal
              id="combo-box-demo"
              options={penumpang}
              sx={{ width: 210.75 }}
              renderInput={(params) => <TextField {...params} label="Jumlah Penumpang" />}
            />
          </div>

          <div>
            <Button className="search-car">Cari Mobil</Button>
          </div>
        </div>
      </section>
      <Footer />
    </>
  )
}

export default Car